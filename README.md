# MT940 statement file parser

A Clojure library designed to parse MT940 statement files from ABN AMRO, KNAB
and ASN Bank.

## Usage

Use it as a stand-alone application from the command line

`java -jar mt940-edn-0.1.0-SNAPSHOT-standalone.jar dialect filename`

where filename is the path to the MT940 file. Currently supported dialects are:

- ABN AMRO Bank
  `java -jar mt940-edn-0.1.0-SNAPSHOT-standalone.jar abnamro filename`

- KNAB Bank
  `java -jar mt940-edn-0.1.0-SNAPSHOT-standalone.jar knab filename`

- ASN Bank
  `java -jar mt940-edn-0.1.0-SNAPSHOT-standalone.jar asn filename`

Use it as a library by calling the function
`mt940.core/import-statements` with the path to the MT940 file as
argument.

## Example

    ABNANL2A
    940
    ABNANL2A
    :20:ABN AMRO BANK NV
    :25:438463378
    :28:10101/1
    :60F:C140410EUR3473,84
    :61:1404110411D15,21N426NONREF
    :86:BEA   NR:516TLZ   11.04.14/10.12 JUMBO SUPERMARKT  AMSTER,PAS231
    :62F:C140411EUR3458,63
    -
    ABNANL2A
    940
    ABNANL2A
    :20:ABN AMRO BANK NV
    :25:438463378
    :28:11201/1
    :60F:C140417EUR646,99
    :61:1404180422D1,42N426NONREF
    :86:BEA   NR:58LTJ4   18.04.14/12.19 JAN'S LUNCHROOM  AMSTERD,PAS231
    :61:1404190422D2,85N426NONREF
    :86:BEA   NR:6RM280   19.04.14/14.40 PRAXIS MEGASTORE  AMSTER,PAS231
    :61:1404220422D8,1N526NONREF
    :86:ABN AMRO BANK N.V.               PENSIOENAANV.               5,00
    MAANDBIJDRAGE               3,10 HEEFT U INTERNETBANKIEREN DAN IS
    UW NOTA BESCHIKBAAR ONDER TOOLS, DOWNLOADEN, AFSCHRIFTEN
    OF U ONTVANGT DEZE PER POST.
    :61:1404190422D17,96N426NONREF
    :86:BEA   NR:7BHH82   19.04.14/14.30 1510 ACTION    AMSTERDAM,PAS231
    :62F:C140422EUR616,66
    -

This example consists of two statements, the first statement contains one
transaction, the second statement contains three transactions.

Running mt940-edn will result in the following output:

```clojure
[{:bank-name "ABN AMRO BANK NV",
  :account-id "438463378",
  :statement-id "10101/1",
  :opening-balance
  {:date #object[java.time.LocalDate 0x670c171c "2014-04-11"],
   :amount 86846/25,
   :currency "EUR"},
  :transactions
  [{:terminal-id "516TLZ",
    :amount -1521/100,
    :entry-date #object[java.time.LocalDate 0x3eb292cd "2014-04-11"],
    :card-id 231,
    :transaction-type "BEA",
    :value-date #object[java.time.LocalDate 0x3c4ad54 "2014-04-11"],
    :timestamp
    #object[java.time.LocalDateTime 0x52b46d52 "2014-04-11T10:12"],
    :payee-id "JUMBO SUPERMARKT AMSTER",
    :memo
    "BEA   NR:516TLZ   11.04.14/10.12 JUMBO SUPERMARKT  AMSTER,PAS231"}],
  :closing-balance
  {:date #object[java.time.LocalDate 0x2fa47368 "2014-04-12"],
   :amount 345863/100,
   :currency "EUR"}}
 {:bank-name "ABN AMRO BANK NV",
  :account-id "438463378",
  :statement-id "11201/1",
  :opening-balance
  {:date #object[java.time.LocalDate 0x2f0e7fa8 "2014-04-18"],
   :amount 64699/100,
   :currency "EUR"},
  :transactions
  [{:terminal-id "58LTJ4",
    :amount -71/50,
    :entry-date #object[java.time.LocalDate 0x6d67f5eb "2014-04-22"],
    :card-id 231,
    :transaction-type "BEA",
    :value-date #object[java.time.LocalDate 0x351e86b2 "2014-04-18"],
    :timestamp
    #object[java.time.LocalDateTime 0x6088451e "2014-04-18T12:19"],
    :payee-id "JAN'S LUNCHROOM AMSTERD",
    :memo
    "BEA   NR:58LTJ4   18.04.14/12.19 JAN'S LUNCHROOM  AMSTERD,PAS231"}
   {:terminal-id "6RM280",
    :amount -57/20,
    :entry-date #object[java.time.LocalDate 0x42f85fa4 "2014-04-22"],
    :card-id 231,
    :transaction-type "BEA",
    :value-date #object[java.time.LocalDate 0x24ac6fef "2014-04-19"],
    :timestamp
    #object[java.time.LocalDateTime 0x2c63762b "2014-04-19T14:40"],
    :payee-id "PRAXIS MEGASTORE AMSTER",
    :memo
    "BEA   NR:6RM280   19.04.14/14.40 PRAXIS MEGASTORE  AMSTER,PAS231"}
   {:value-date #object[java.time.LocalDate 0x5a63fa71 "2014-04-22"],
    :entry-date #object[java.time.LocalDate 0x79777da7 "2014-04-22"],
    :amount -81/10,
    :description
    "ABN AMRO BANK N.V. PENSIOENAANV. 5,00 MAANDBIJDRAGE 3,10 HEEFT U INTERNETBANKIEREN DAN IS UW NOTA BESCHIKBAAR ONDER TOOLS, DOWNLOADEN, AFSCHRIFTEN OF U ONTVANGT DEZE PER POST.",
    :memo
    "ABN AMRO BANK N.V.               PENSIOENAANV.               5,00\nMAANDBIJDRAGE               3,10 HEEFT U INTERNETBANKIEREN DAN IS\nUW NOTA BESCHIKBAAR ONDER TOOLS, DOWNLOADEN, AFSCHRIFTEN\nOF U ONTVANGT DEZE PER POST."}
   {:terminal-id "7BHH82",
    :amount -449/25,
    :entry-date #object[java.time.LocalDate 0x1b410308 "2014-04-22"],
    :card-id 231,
    :transaction-type "BEA",
    :value-date #object[java.time.LocalDate 0x51f4439e "2014-04-19"],
    :timestamp
    #object[java.time.LocalDateTime 0x6870c3c2 "2014-04-19T14:30"],
    :payee-id "1510 ACTION AMSTERDAM",
    :memo
    "BEA   NR:7BHH82   19.04.14/14.30 1510 ACTION    AMSTERDAM,PAS231"}],
  :closing-balance
  {:date #object[java.time.LocalDate 0x59b3f754 "2014-04-23"],
   :amount 30833/50,
   :currency "EUR"}}]
```

The data structure is a vector of maps, where each map is a statement.
The map contains another vector of maps inside :transactions, where each
map is a transaction.

## License

Copyright © 2019 Stanley Jaddoe

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
