(ns mt940-edn.memo
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str :refer [blank? split starts-with?]]
   [mt940-edn.util :as u]))

(defn data-str
  ([s] (u/clean-string (u/remove-newlines s)))
  ([s & srest] (data-str (apply str s srest))))

(defn starts-with-re? [s re]
  (some? (re-find re s)))

(def dotall {:regex "(?s)"})
(def whitespace {:regex "\\s*"})

(defn split-at-32 [s]
  (let [n 32]
    (split (if (and (>= (count s) (inc n)) (= (nth s n) \space))
             (str (doto (StringBuilder. s) (. setCharAt n \newline)))
             s) #"\n")))

;--------------------------------------------------------
; SEPA - common utility functions
;--------------------------------------------------------

(defn sepa-ideal->timestamp [reference]
  (let [digits (re-seq #"\d" reference)
        timestamp (apply str (take 12 digits))]
    {:timestamp (u/local-date-time "ddMMyyyyHHmm" timestamp)
     :payment-reference (apply str (drop 12 digits))}))

(defn sepa->transaction-type [sepa-transaction-type]
  (get {"SEPA PERIODIEKE OVERB." "SEPA PERIODIEKE OVERBOEKING"
        "SEPA IDEAL" "IDEAL"
        "SEPA ACCEPTGIROBETALING" "ACCEPTGIROBETALING"
        "IDEAL BETALING" "IDEAL"
        "SEPA ACCEPTGIRO" "ACCEPTGIROBETALING"}
       sepa-transaction-type
       sepa-transaction-type))

;--------------------------------------------------------
; SEPA (unstructured)
;--------------------------------------------------------

(def sepa-unstructured-keywords
  {"SEPA" :transaction-type
   "IBAN" :payee-id
   "BIC" :creditor-bank-id
   "NAAM" :payee-name
   "OMSCHRIJVING" :description
   "BETALINGSKENM." :payment-reference
   "KENMERK" :reference
   "INCASSANT" :kvk
   "MACHTIGING" :mandate})

(def sepa-unstructured-keywords-pattern
  (->> (keys sepa-unstructured-keywords)
       (str/join "|")
       (format "^(%s): (.*?)$")
       (re-pattern)))

(defn sepa-unstructured-preprocess [memo]
  (->> memo str/split-lines
       (eduction
        (mapcat split-at-32)
        (map #(str/replace % #"\s\s+" " \n"))
        (mapcat str/split-lines))))

(defn sepa-unstructured-postprocess [parsed-memo]
  (-> parsed-memo
      (update :transaction-type sepa->transaction-type)
      ((fn [{:keys [transaction-type reference] :as parsed-memo}]
         (cond-> parsed-memo
           (and (= transaction-type "IDEAL") (some? reference))
           (merge {:reference nil} (sepa-ideal->timestamp reference)))))))

(defn sepa-unstructured [memo-lines]
  (->> memo-lines
       sepa-unstructured-preprocess
       (reduce
        (fn [[[prev-kw prev-value] & res] s]
          (if-let [[_ kw value] (re-find sepa-unstructured-keywords-pattern s)]
            (conj res [prev-kw prev-value] [(get sepa-unstructured-keywords kw) value])
            (conj res [prev-kw (str prev-value s)])))
        '([:transaction-type]))
       (into {} (map #(update % 1 data-str)))
       sepa-unstructured-postprocess))

;--------------------------------------------------------
; SEPA (new style)
;--------------------------------------------------------

(def sepa-keywords
  {"TRTP" :transaction-type
   "IBAN" :payee-id
   "BBAN" :account-id
   "BIC"  :creditor-bank-id
   "NAME" :payee-name
   "REMI" :description
   "EREF" :payment-reference
   "CSID" :kvk
   "MARF" :mandate
   "ORDP-ID"   :ordering-party-id
   "ORDP-NAME" :ordering-party-name
   "BENM-ID"   :beneficiary-id
   "BENM-NAME" :beneficiary-name})

(defn sepa->tag-data [[[tag] data]]
  [(get sepa-keywords tag)
   (data-str (str/join "/" data))])

(defn sepa-postprocess [parsed-memo]
  (-> parsed-memo
      (update :transaction-type sepa->transaction-type)
      ((fn [{:keys [transaction-type payment-reference] :as parsed-memo}]
         (cond-> parsed-memo
           (and (= transaction-type "IDEAL") (some? payment-reference))
           (merge (sepa-ideal->timestamp payment-reference)))))))

(defn sepa [memo]
  (sepa-postprocess
   (into {}
         (comp
          (partition-by #(contains? sepa-keywords %))
          (drop 1)
          (partition-all 2)
          (map sepa->tag-data)
          (remove #(empty? (second %))))
         (-> memo
             (str/replace #"/(ORDP|BENM)//(NAME|ID)/" "/$1-$2/")
             (split #"/")))))

;--------------------------------------------------------
; ATM / Payment terminal
;--------------------------------------------------------

(def terminal-matcher
  (u/regex-elements
   [dotall
    {:id :transaction-type
     :regex "(GEA|BEA)"}
    {:regex "\\s{3,12}(NR[: ])?"}
    {:id :terminal-id
     :regex "([A-Z0-9]{6,8})?"
     :keep? #(not (blank? %))}
    {:regex "\\s{1,3}"}
    {:id :timestamp
     :regex "\\d\\d\\.\\d\\d\\.\\d\\d\\/\\d\\d\\.\\d\\d"
     :transform (partial u/local-date-time "dd.MM.yy/HH.mm")}
    {:id :payee-name
     :regex "[\\s\\p{Graph}]{1,26}"
     :transform u/clean-string}
    {:regex ",PAS"}
    {:id :card-id
     :regex "\\d{3}"
     :transform edn/read-string}
    {:id :description
     :regex ".*"
     :transform u/clean-string
     :keep? #(not (blank? %))}]))

(def terminal-matcher-betaalpas
  (u/regex-elements
   [dotall
    {:id :transaction-type
     :regex "(GEA|BEA)"}
    {:regex ", BETAALPAS\\s+"}
    {:id :payee-name
     :regex "[\\s\\p{Graph}]{1,26}"
     :transform u/clean-string}
    {:regex ",PAS"}
    {:id :card-id
     :regex "\\d{3}"
     :transform edn/read-string}
    {:regex "\\n(NR[: ])?"}
    {:id :terminal-id
     :regex "([A-Z0-9]{6,8})?"
     :keep? #(not (blank? %))}
    {:regex ",?\\s{1,3}"}
    {:id :timestamp
     :regex "\\d\\d\\.\\d\\d\\.\\d\\d\\/\\d\\d[:\\.]\\d\\d"
     :transform u/parse-timestamp}
    {:id :description
     :regex ".*"
     :transform u/clean-string
     :keep? #(not (blank? %))}]))

(defn terminal [memo]
  (if (str/includes? memo ", BETAALPAS")
    (u/get-matches terminal-matcher-betaalpas memo)
    (u/get-matches terminal-matcher memo)))

;--------------------------------------------------------
; Payee and description
;--------------------------------------------------------

(defn payee-and-description-preprocess [s]
  (cond
    (and (starts-with? s "  ") (= (nth s 33) \space))
    (mapcat split-at-32 (split (subs s 1) #"\n"))

    (starts-with? s "  ")
    (->> (str/replace (subs s 1) #"\n" "")
         (partition 32)
         (map (partial apply str)))

    (starts-with? s " ")
    (split s #"\n")

    (starts-with? s "GIRO ")
    (let [s (str/replace-first s #"GIRO( +)" "$1GIRO")]
      (cond
        (str/includes? (subs s 0 (min (count s) 33)) "\n")
        (split s #"\n")

        (= (nth s 32) \space)
        (mapcat split-at-32 (split s #"\n"))

        :else
        (->> (str/replace s #"\n" "")
             (partition 32)
             (map (partial apply str)))))

    :else
    (mapcat split-at-32 (split (str " " s) #"\n"))))

(defn payee-and-description-address [lines]
  (let [[street-po-box pc-city & remaining-lines] lines]
    (if (and pc-city (starts-with-re? pc-city #"^\d{4} [A-Z]{2} "))
      {:address [street-po-box pc-city]
       :lines remaining-lines}
      {:lines lines})))

(defn payee-and-description [memo]
  (let [[line1 line2 & lines] (map u/clean-string (payee-and-description-preprocess memo))
        [payee-id payee-name] (split line1 #" +" 2)
        lines (if (blank? payee-name) lines (conj lines line2))
        payee-name (if (blank? payee-name) line2 payee-name)
        {:keys [address lines]} (payee-and-description-address lines)
        lines (remove blank? lines)]
    (cond-> {:payee-id payee-id
             :payee-name payee-name}
      (some? address)
      (assoc :payee-address (str/join "\n" address))

      (not-empty lines)
      (assoc :description (str/join "\n" lines)))))

;--------------------------------------------------------
; ASN Bank
;--------------------------------------------------------

(defn- asn->key-value-pairs [description]
  (letfn [(split->> [p s] (split s p))]
    (->> description
         u/clean-string
         (split->> #"-")
         (reduce
          (fn [res kv-or-remainder]
            (let [[k v] (split kv-or-remainder #": ")
                  [k v] (if v [k v] [nil k])
                  field (get {"Europese incasso" :description
                              "Factuur" :reference
                              "Incassant ID" :kvk
                              "Kenmerk Machtiging" :mandate}
                             k)]
              (if field
                (conj res [field v])
                (update-in res [(dec (count res)) 1] str "-" k v))))
          [])
         (into {}))))

(defn- asn-reference [memo-without-payee-line]
  (let [[before-refs timestamp+ref] (split memo-without-payee-line #" Referentie: " 2)
        [ref1 ref2 & payee-description] (split before-refs #" ")
        [_date _time ref] (split timestamp+ref #" " 3)]
    {:reference ref1
     :reference2 ref2
     :description (str/join " " payee-description)
     :timestamp (u/parse-timestamp timestamp+ref)
     :payment-reference ref}))

(defn asn-memo [memo]
  (let [[payee-line _empty-line & desc-lines] (split memo #"\n")
        description (u/clean-string (apply str desc-lines))
        description-by-dash (split description #"-")
        [payee-id payee-name] (split payee-line #" " 2)]
    (cond-> {:description description}

      (not (blank? payee-id))
      (assoc :payee-id payee-id)

      (not (blank? payee-name))
      (assoc :payee-name payee-name)

      (= (second description-by-dash) payee-id)
      (assoc :description (str/join "-" (drop 3 description-by-dash))
             :payment-reference (first description-by-dash))

      (= (first description-by-dash) payee-id)
      (assoc :description (str/join "-" (drop 2 description-by-dash)))

      (str/includes? description " Referentie: ")
      (merge (asn-reference description))

      (str/starts-with? description "Europese incasso:")
      (merge (asn->key-value-pairs description)))))

;--------------------------------------------------------
; Misc./fallback
;--------------------------------------------------------

(defn misc-memo [memo]
  {:description (u/clean-string memo)})

;--------------------------------------------------------
; Parser dispatching (peek and use most suitable parser)
;--------------------------------------------------------

(defn parse-transaction-memo [dialect mt940-memo]
  (let [m (subs mt940-memo 0 (min (count mt940-memo) 6))
        parse-fn (cond
                   (= (:id dialect) :asn) asn-memo
                   (< (count m) 6) misc-memo
                   (starts-with? m "SEPA ") sepa-unstructured
                   (starts-with? m "/TRTP/") sepa
                   (starts-with? m "BEA") terminal
                   (starts-with? m "GEA") terminal
                   (starts-with-re? m #"^GIRO|^(\s*\d\d\.)") payee-and-description
                   :else misc-memo)]
    (assoc (parse-fn mt940-memo) :memo mt940-memo)))
