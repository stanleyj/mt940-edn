(ns mt940-edn.statement
  (:require
   [clojure.string :as str]
   [mt940-edn.balance-info :as bi]
   [mt940-edn.memo :as m]
   [mt940-edn.mutation-info :as mi]))

;--------------------------------------------------------
; Translate raw tags and data into generic statement tags
;--------------------------------------------------------

(defn- tag->generic [dialect [t d]]
  (case t
    :20  [:bank-name (:bank-name dialect d)]
    :25  [:account-id d]
    :28  [:statement-id d]
    :28C [:statement-id d]              ; KNAB
    :60F [:opening-balance (bi/parse-statement-balance-info d)]
    :62F [:closing-balance (bi/parse-statement-balance-info d)]
    :61  [:transaction-mutation (mi/parse-transaction-mutation-info dialect d)]
    :86  [:transaction-memo (m/parse-transaction-memo dialect d)]
    [t d]))

(defn tags->generic [dialect s]
  (for [[_ tag data _] (re-seq #"(?s):(\d\d[A-Z]?):(.+?)(?=(?:\n:(\d\d[A-Z]?)):|$)" s)]
    (tag->generic dialect [(keyword tag) data])))

(defn- group-transactions [gs [t d]]
  (case t
    :transaction-memo
    (update-in gs [:transactions (dec (count (:transactions gs)))] merge d)
    :transaction-mutation
    (update gs :transactions (fnil conj []) d) ; append to vector
    ;; else
    (assoc gs t d)))

(defn generic-statement [generic-statement-tags]
  (reduce group-transactions {} generic-statement-tags))

;--------------------------------------------------------
; Process raw MT940 files (e.g. downloaded from ABN-AMRO)
;--------------------------------------------------------

(defn parse-mt940-lines [dialect lines]
  (into []
        (comp (partition-by #(= % (:statement-separator dialect "-"))) ; group lines by statement
              (take-nth 2) ; remove the statement separator from the results
              (map (partial str/join "\n")) ; ensure multi-line data is joined
              (map (partial tags->generic dialect)) ; translate tag and data to generic statement tags
              (map generic-statement)) ; create generic statement with transactions
        lines))
