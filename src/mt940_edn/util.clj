(ns mt940-edn.util
  (:require [clojure.edn :as edn]
            [clojure.string :as str])
  (:import
   [java.time LocalDate LocalDateTime]
   [java.time.format DateTimeFormatter]))

(defn local-date
  ([fmt date-str]
   (LocalDate/parse date-str (DateTimeFormatter/ofPattern fmt)))
  ([y m d]
   (LocalDate/of y m d)))

(defn local-date-time
  ([fmt datetime-str]
   (LocalDateTime/parse datetime-str (DateTimeFormatter/ofPattern fmt)))
  ([y m d hh mm]
   (LocalDateTime/of y m d hh mm)))

(defn abs [n] (max n (- n)))

(defn parse-amount [amount-string]
  (-> amount-string
      (str/replace "D" "-")
      (str/replace "C" "")
      (str/replace "," ".")
      (edn/read-string)
      (rationalize)))

(defn parse-date [date-string]
  (cond
    (= date-string "null") nil
    (= 6 (count date-string)) (local-date "yyMMdd" date-string)
    :else (local-date "yyyyMMdd" date-string)))

(defn parse-timestamp [s]
  (or (some->> s
               (re-find #"\d\d\d\d-\d\d\-\d\d \d\d:\d\d")
               (local-date-time "yyyy-MM-dd HH:mm"))
      (some->> s
               (re-find #"\d\d\d\d-\d\d\-\d\dT\d\d:\d\d")
               (local-date-time "yyyy-MM-dd'T'HH:mm"))
      (some->> s
               (re-find #"\d\d-\d\d\-\d\d\d\d \d\d:\d\d")
               (local-date-time "dd-MM-yyyy HH:mm"))
      (when-let [[d m y hh mm] (map #(Integer/parseInt %) (re-seq #"\d{2}" s))]
        (local-date-time (+ y 2000) m d hh mm))))

(defn remove-newlines [data]
  (str/replace data "\n" ""))

(defn clean-string [data]
  (-> data
      (str/trim)
      (str/replace #"\s+" " ")))

;--------------------------------------------------------
; Regex parser builder
;--------------------------------------------------------

(defn- id->group-name [id]
  (-> (name id)
      str/lower-case
      (str/replace #"[^a-z0-9]" "")))

(defn- element->regex [{:keys [id regex]}]
  (if-not id regex
          (format "(?<%s>%s)" (id->group-name id) regex)))

(defn make-regex [elements]
  (re-pattern (apply str (map element->regex elements))))

(defn get-matches [{:keys [matcher elements post-process]} parse-input]
  (let [result (matcher parse-input)]
    (if-not (.matches result)
      (throw (Exception. (format "Could not parse:\n%s\nwith:\n%s" parse-input (make-regex elements))))
      (post-process
       (into {} (for [{:keys [id transform keep?] :or {transform identity keep? some?}} elements
                      :when id
                      :let [value (transform (.group result (id->group-name id)))]
                      :when (keep? value)]
                  [id value]))))))

(defmacro regex-elements
  ([elements]
   `(regex-elements ~elements identity))
  ([elements post-process-fn]
   `{:elements ~elements
     :post-process ~post-process-fn
     :matcher (partial re-matcher (make-regex ~elements))}))
